import axios from "axios";

const axiosInstance = () => {
    const defaultOptions = {
        baseURL: "WRITE_SERVER_API_URL_HERE",
    };

    // Create instance
    let instance = axios.create(defaultOptions);

    // Set the config for any request
    instance.interceptors.request.use(function (config) {
        return config;
    });

    instance.interceptors.response.use(function (response) {
        return response;
    });

    return instance;
};

export default axiosInstance();
