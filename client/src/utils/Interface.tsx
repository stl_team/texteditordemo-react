export interface WFobject {
    id: number;
    title: any;
}

export interface Headerpro {
    size: number;
    line: number;
    space: number;
    align: any;
    weight: WFobject;
    familys: WFobject;
    fontcolor: string;
    content: string;
    setContent: (content: string) => void;
}

export interface Options {
    id: number;
    title: string;
}

export interface IProps {
    className: string;
    variant: any;
    value: any;
    options: Options[];
    onChange: any;
    label: string;
}

export interface InputProps {
    name: string;
    label: string;
    type: string;
    variant: any;
    InputProps: any;
    inputRef: any;
    onChange: any;
    value: any;
    inputProps: any;
}

export interface Sidebarpro {
    size: number;
    setSize: (size: number) => void;
    setLine: (line: number) => void;
    line: number;
    setSpace: (space: number) => void;
    space: number;
    setAlign: (align: string) => void;
    align: any;
    setweight: (weight: WFobject) => void;
    weight: any;
    setFamilys: (fontFamily: WFobject) => void;
    familys: any;
    setFontColor: (fontcolor: string) => void;
    fontcolor: string;
    content: string;
}
