import { Options } from "../../utils/Interface";

export const fontList: Options[] = [
    { title: "Roboto", id: 0 },
    { title: "Fira Sans", id: 1 },
    { title: "Lato", id: 2 },
    { title: "Montserrat", id: 3 },
    { title: "Roboto Condensed", id: 4 },
    { title: "Source Sans Pro", id: 5 },
    { title: "Oswald", id: 6 },
    { title: "Merriweather", id: 7 },
    { title: "PT Sans", id: 8 },
    { title: "Roboto Slab", id: 9 },
];

export const fontStyle: Options[] = [
    { title: "normal", id: 1 },
    { title: "bolder", id: 2 },
    { title: "lighter", id: 3 },
    { title: "bold", id: 4 },
    { title: "inherit", id: 5 },
    { title: "unset", id: 6 },
    { title: "revert", id: 7 },
];
