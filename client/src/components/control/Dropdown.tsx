import { FormControl } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { IProps } from "../../utils/Interface";

const DropDown = (props: IProps) => {
    const { value, className, options, variant, onChange, label } = props;
    return (
        <>
            <FormControl variant={variant} className={className}>
                <Autocomplete
                    id="combo-box-demo"
                    options={options}
                    getOptionLabel={(option) => option.title}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label={label}
                            variant="outlined"
                        />
                    )}
                    onChange={onChange}
                    value={value}
                />
            </FormControl>
        </>
    );
};

export default DropDown;
