import { TextField } from "@material-ui/core";
import { InputProps } from "../../utils/Interface";

const Input = (props: InputProps) => {
    const {
        name,
        label,
        type,
        variant,
        InputProps,
        inputRef,
        onChange,
        value,
        inputProps,
    } = props;
    return (
        <TextField
            name={name}
            label={label}
            type={type}
            variant={variant}
            InputProps={InputProps}
            inputRef={inputRef}
            onChange={onChange}
            value={value}
            inputProps={inputProps}
        />
    );
};

export default Input;
