import { useForm } from "react-hook-form";
import {
    Grid,
    InputAdornment,
    FormControl,
    Button,
    makeStyles,
    IconButton,
} from "@material-ui/core";

import {
    FormatLineSpacing as FormatLineSpacingIcon,
    FormatSize as FormatSizeIcon,
    FormatColorText as FormatColorTextIcon,
    FormatAlignLeft as FormatAlignLeftIcon,
    FormatAlignCenter as FormatAlignCenterIcon,
    FormatAlignJustify as FormatAlignJustifyIcon,
    FormatAlignRight as FormatAlignRightIcon,
} from "@material-ui/icons";
import styled from "styled-components";
import InputColor from "react-input-color";
import { useToasts } from "react-toast-notifications";

import { fontList, fontStyle } from "./control/Services";
import axios from "../utils/ApiConfig";
import DropDown from "./control/Dropdown";
import Input from "./control/Input";
import { Sidebarpro } from "../utils/Interface";

const Sidemenu = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    right: 0px;
    width: 320px;
    height: 100vh;
    background-color: #ffffff;
`;

const useStyle = makeStyles({
    dropdown: {
        width: "100%",
    },
    textfiled: {},
    gridpl: {
        padding: 5,
    },
});

const Sidebar = (props: Sidebarpro) => {
    const classes = useStyle();
    const {
        size,
        setSize,
        setLine,
        line,
        setSpace,
        space,
        setAlign,
        align,
        setweight,
        weight,
        setFamilys,
        familys,
        setFontColor,
        fontcolor,
        content,
    } = props;
    const { register, handleSubmit } = useForm();
    const { addToast } = useToasts();

    const onSubmit = async () => {
        const params = {
            font_family: familys.title,
            font_weight: weight.title,
            font_size: size ? size : 16,
            font_color: fontcolor,
            line_space: line ? line : 2,
            word_space: space ? space : 2,
            alignment: align,
            content: content,
        };
        await axios
            .post(`/Add`, params)
            .then((res) => {
                if (res?.data?.status === 200) {
                    addToast(res?.data?.message, {
                        appearance: "success",
                        autoDismiss: true,
                    });
                }
            })
            .catch((error) => {
                if (
                    error?.response?.data &&
                    error?.response?.data?.status === 400
                ) {
                    let vals = Object.values(error?.response?.data?.data);
                    let validationMsgs = vals.join();
                    addToast(validationMsgs, {
                        appearance: "error",
                        autoDismiss: true,
                    });
                } else {
                    addToast(error?.response?.data?.message, {
                        appearance: "error",
                        autoDismiss: true,
                    });
                }
            });
    };

    return (
        <Sidemenu>
            <form
                autoComplete="off"
                className="form"
                onSubmit={handleSubmit(onSubmit)}
            >
                Text
                <Grid container>
                    <Grid item xs={12} className={classes.gridpl}>
                        <DropDown
                            variant="outlined"
                            className={classes.dropdown}
                            onChange={(
                                e: React.ChangeEvent<HTMLInputElement>,
                                values: any
                            ) => {
                                values !== null && setFamilys(values);
                            }}
                            value={familys}
                            options={fontList}
                            label="Font Family"
                        />
                    </Grid>
                    <Grid item xs={8} className={classes.gridpl}>
                        <DropDown
                            variant="outlined"
                            className={classes.dropdown}
                            onChange={(
                                e: React.ChangeEvent<HTMLInputElement>,
                                values: any
                            ) => {
                                values !== null && setweight(values);
                            }}
                            value={weight}
                            options={fontStyle}
                            label="Font Weight"
                        />
                    </Grid>
                    <Grid item xs={4} className={classes.gridpl}>
                        <Input
                            name="size"
                            label="size"
                            type="number"
                            variant="outlined"
                            value={size}
                            inputRef={register}
                            onChange={(
                                e: React.ChangeEvent<HTMLInputElement>
                            ) => {
                                setSize(
                                    e.target?.value !== ""
                                        ? parseInt(e.target?.value) > 50
                                            ? 50
                                            : parseInt(e.target?.value) < 1
                                            ? 1
                                            : parseInt(e.target.value)
                                        : 1
                                );
                            }}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <FormatSizeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            inputProps={{ min: "1", max: "50", step: "1" }}
                        />
                    </Grid>

                    <Grid item xs={12} className={classes.gridpl}>
                        <FormControl variant="outlined" fullWidth>
                            <InputColor
                                initialValue={fontcolor}
                                onChange={(e) => setFontColor(e.hex)}
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={6} className={classes.gridpl}>
                        <Input
                            name="per"
                            label="per"
                            type="number"
                            variant="outlined"
                            value={line}
                            inputRef={register}
                            onChange={(
                                e: React.ChangeEvent<HTMLInputElement>
                            ) => {
                                setLine(
                                    e.target?.value !== ""
                                        ? parseInt(e.target?.value) > 50
                                            ? 50
                                            : parseInt(e.target?.value) < 1
                                            ? 1
                                            : parseInt(e.target.value)
                                        : 1
                                );
                            }}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <FormatLineSpacingIcon />
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="end">
                                        %
                                    </InputAdornment>
                                ),
                            }}
                            inputProps={{ min: 1, max: 50, step: "any" }}
                        />
                    </Grid>
                    <Grid item xs={6} className={classes.gridpl}>
                        <Input
                            name="px"
                            label="px"
                            type="number"
                            variant="outlined"
                            value={space}
                            inputRef={register}
                            onChange={(
                                e: React.ChangeEvent<HTMLInputElement>
                            ) => {
                                setSpace(
                                    e.target?.value !== ""
                                        ? parseInt(e.target?.value) > 500
                                            ? 500
                                            : parseInt(e.target?.value) < 1
                                            ? 1
                                            : parseInt(e.target.value)
                                        : 1
                                );
                            }}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <FormatColorTextIcon />
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="end">
                                        px
                                    </InputAdornment>
                                ),
                            }}
                            inputProps={{ min: "1", max: "50", step: "any" }}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <IconButton onClick={() => setAlign("left")}>
                            <FormatAlignLeftIcon
                                color={
                                    align === "left" ? "secondary" : "inherit"
                                }
                            />
                        </IconButton>
                    </Grid>
                    <Grid item xs={3}>
                        <IconButton onClick={() => setAlign("center")}>
                            <FormatAlignCenterIcon
                                color={
                                    align === "center" ? "secondary" : "inherit"
                                }
                            />
                        </IconButton>
                    </Grid>
                    <Grid item xs={3}>
                        <IconButton onClick={() => setAlign("right")}>
                            <FormatAlignRightIcon
                                color={
                                    align === "right" ? "secondary" : "inherit"
                                }
                            />
                        </IconButton>
                    </Grid>
                    <Grid item xs={3}>
                        <IconButton onClick={() => setAlign("justify")}>
                            <FormatAlignJustifyIcon
                                color={
                                    align === "justify"
                                        ? "secondary"
                                        : "inherit"
                                }
                            />
                        </IconButton>
                    </Grid>
                    <Grid item xs={12} className={classes.gridpl}>
                        <Button
                            variant="contained"
                            fullWidth
                            type="submit"
                            color="primary"
                        >
                            Save Changes
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Sidemenu>
    );
};

export default Sidebar;
