import { useState, useEffect, useCallback } from "react";
import Middle from "./Middle";
import Sidebar from "./Sidebar";
import axios from "../utils/ApiConfig";
import { fontList, fontStyle } from "./control/Services";

const Index = () => {
    const textContent =
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

    const [size, setSize] = useState(16);
    const [line, setLine] = useState(2);
    const [space, setSpace] = useState(2);
    const [align, setAlign] = useState("justify");
    const [weight, setweight] = useState(fontStyle[0]);
    const [familys, setFamilys] = useState(fontList[0]);
    const [fontcolor, setFontColor] = useState("#0a0a0aff");
    const [content, setContent] = useState(textContent);

    const getContentData = useCallback(async () => {
        let res = await axios.get(`/Return`);
        let resData = res?.data?.data;
        if (resData) {
            let newWeight = fontStyle.filter((val) => {
                return val.title === resData.font_weight;
            });

            let fontFamily = fontList.filter((val) => {
                return val.title === resData.font_family;
            });
            setSize(resData.font_size === null ? size : resData.font_size);
            setLine(resData.line_space === null ? line : resData.line_space);
            setSpace(resData.word_space === null ? space : resData.word_space);
            setAlign(resData.alignment === null ? align : resData.alignment);
            setweight(newWeight && newWeight[0]);
            setFamilys(fontFamily && fontFamily[0]);
            setFontColor(
                resData.font_color === null ? fontcolor : resData.font_color
            );
            setContent(resData.content === null ? content : resData.content);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        getContentData();
    }, [getContentData]);

    return (
        <>
            <Middle
                size={size}
                line={line}
                space={space}
                align={align}
                weight={weight}
                familys={familys}
                fontcolor={fontcolor}
                content={content}
                setContent={setContent}
            />
            <Sidebar
                size={size}
                setSize={setSize}
                setLine={setLine}
                line={line}
                setSpace={setSpace}
                space={space}
                setAlign={setAlign}
                align={align}
                setweight={setweight}
                weight={weight}
                setFamilys={setFamilys}
                familys={familys}
                setFontColor={setFontColor}
                fontcolor={fontcolor}
                content={content}
            />
        </>
    );
};

export default Index;
