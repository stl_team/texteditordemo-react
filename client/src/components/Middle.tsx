import { makeStyles, Card, CardContent, TextField } from "@material-ui/core";
import { Headerpro } from "../utils/Interface";

const Header = (props: Headerpro) => {
    const {
        size,
        line,
        space,
        align,
        weight,
        familys,
        fontcolor,
        content,
        setContent,
    } = props;

    const useStyles = makeStyles({
        root: {
            maxWidth: 500,
            margin: "auto",
            marginTop: "170px",
        },
        content: {
            display: "flex",
            flexDirection: "column",
            alignContent: "flex-start",
            minWidth: 500,
        },
        textAreas: {
            fontSize: size ? size : 16,
            lineHeight: line,
            wordSpacing: space,
            textAlign: align,
            fontWeight: weight.title,
            fontFamily: familys.title,
            color: fontcolor,
        },
    });

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card>
                <CardContent className={classes.content}>
                    <TextField
                        label="Text"
                        type="text"
                        multiline
                        rows={20}
                        rowsMax={20}
                        name="text"
                        variant="outlined"
                        fullWidth
                        value={content}
                        inputProps={{
                            shrink: true,
                            className: classes.textAreas,
                        }}
                        onChange={(e) => setContent(e.target.value)}
                    />
                </CardContent>
            </Card>
        </div>
    );
};

export default Header;
