import "./App.css";
import Index from "../components/Index";
import {
    CssBaseline,
    createMuiTheme,
    makeStyles,
    ThemeProvider,
} from "@material-ui/core";
import { ToastProvider } from "react-toast-notifications";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#333996",
            light: "#3c44b126",
        },
        secondary: {
            main: "#f83245",
            light: "#f8324526",
        },
        background: {
            default: "#f4f5fd",
        },
    },
    overrides: {
        MuiAppBar: {
            root: {
                transform: "tranlateZ(0)",
            },
        },
    },
    props: {
        MuiIconButton: {
            disableRipple: true,
        },
    },
});

const useStyle = makeStyles({
    appMain: {
        width: "100%",
        display: "flex",
        flexWrap: "wrap",
    },
});

function App() {
    const classses = useStyle();

    return (
        <ThemeProvider theme={theme}>
            <ToastProvider>
                <div className={classses.appMain}>
                    <Index />
                </div>
            </ToastProvider>
            <CssBaseline />
        </ThemeProvider>
    );
}

export default App;
