<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrontendData extends Model
{
    use HasFactory;

    protected $table = 'frontend_data';
}
