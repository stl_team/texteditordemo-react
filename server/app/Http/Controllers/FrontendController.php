<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FrontendData;

class FrontendController extends Controller
{
    public function Add(Request $request)
    {
        try {
            $validator = \Validator::make($request->all(), [
                'content' => 'required',
                'font_family' => 'required',
                'font_weight' => 'required',
                'font_size' => 'required|Numeric',
                'font_color' => 'required',
                'line_space' => 'required|Numeric',
                'word_space' => 'required|Numeric',
                'alignment' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Validation Failed',
                    'data' => $validator->messages()
                ], 400);
            }

            $content = FrontendData::first();
            $message = "";
            if ($content) {
                $message = "Data updated successfully.";
            } else {
                $message = "Data inserted successfully.";
                $content = new FrontendData();
            }
            $content->content = $request->content;
            $content->font_family = $request->font_family;
            $content->font_weight = $request->font_weight;
            $content->font_size = $request->font_size;
            $content->font_color = $request->font_color;
            $content->line_space = $request->line_space;
            $content->word_space = $request->word_space;
            $content->alignment = $request->alignment;
            if (!$content->save()) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Something Went Wrong',
                    'data' => []
                ], 400);
            }
            return response()->json([
                'status' => 200,
                'message' => $message,
                'data' => $content
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'status' => 500,
                'message' => 'Internal Server Error',
                'data' => $error
            ], 500);
        }
    }
    public function ReturnContent()
    {
        try {
            $data = FrontendData::first();
            if (!$data) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Data not Found',
                    'data' => []
                ], 200);
            }
            return response()->json([
                'status' => 200,
                'message' => 'Data Found',
                'data' => $data
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'status' => 500,
                'message' => 'Internal Server Error',
                'data' => $error
            ], 500);
        }
    }
}
