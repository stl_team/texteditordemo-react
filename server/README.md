### `This is PHP project so need to install all php dependencies.`

## Install Apache2

Paste this link in browser and install.
https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-18-04

## Install MySQL

Paste this link in browser and install.
https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04

## Install PHP

PHP version should be greater than 7.4 and Paste this link in browser and install.
https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-ubuntu-18-04

## Install Composer

You have to install composer for run this project. You can install from this link.
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04

## Install Dependency

Install dependency in your project. Run this command in terminal inside your project directory.

### `composer update`

## ENV

Change your database configuration in your project's .env file.

## Migration

Run this command for create tables in database.

### `php artisan migrate`

Run this command for start your project.

### `php artisan serve`

Your API URL will be

### `http://127.0.0.1:8000/api/`
