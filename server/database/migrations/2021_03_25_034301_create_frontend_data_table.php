<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontendDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frontend_data', function (Blueprint $table) {
            $table->id();
            $table->text('content');
            $table->string('font_family', 50);
            $table->string('font_weight', 20);
            $table->float('font_size');
            $table->string('font_color', 30);
            $table->float('line_space');
            $table->float('word_space');
            $table->string('alignment', 30);
            $table->timestamps();
        });
    }

    /**content
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frontend_data');
    }
}
